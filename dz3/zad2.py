# -*- coding: utf-8 -*-
"""
Created on Mon Mar 16 00:45:34 2020

@author: David
"""

# -*- coding: utf-8 -*-
"""
Created on Sun Mar 15 13:56:48 2020

@author: David
"""


import numpy as np
import pylab as pl
from sklearn import svm
import pandas as pd

#a-------------------------------------------------------------------------
X = pd.read_csv("Podaci/X_a.csv").to_numpy()
Y = pd.read_csv("Podaci/y_a.csv").to_numpy().reshape(-1,)
random=np.random.choice(2000,1000,replace=False)
X=X[random]
Y=Y[random]
#sto manje podataka to veca margina

# fit the model
clf = svm.SVC(kernel='linear', C=100)
clf.fit(X, Y)

# get the separating hyperplane
w = clf.coef_[0]
print("w0: ",w[0],"w1: ",w[1])
#print(w)
k = -w[0] / w[1]
xx = np.linspace(-10, 30)#default len is 50 #za a,b podatke (-10,30),za c podatke (-5,15)
yy = k * xx - (clf.intercept_[0]) / w[1]#0=w0x+w1y => y=-w0x/w1 =>k=-w0x/w1

# plot the parallels to the separating hyperplane that pass through the
# support vectors
bdown = clf.support_vectors_[0]
#print(clf.support_vectors_)
yy_down = k * xx + (bdown[1] - k * bdown[0])
bup = clf.support_vectors_[-1]
yy_up = k * xx + (bup[1] - k * bup[0])
margin=(bup[1] - k * bup[0])-(bdown[1] - k * bdown[0])
print("margina: ",margin)

# plot the line, the points, and the nearest vectors to the plane
pl.set_cmap(pl.cm.Paired)
pl.plot(xx, yy,"black")
pl.plot(xx, yy_down, 'k--')
pl.plot(xx, yy_up, 'k--')

pl.scatter(clf.support_vectors_[:, 0], clf.support_vectors_[:, 1],
           s=80, facecolors='none')
pl.scatter(X[:, 0], X[:, 1], c=Y)

#pl.axis('tight')
pl.show()
#b--------------------------------------------------------------------------------
X = pd.read_csv("Podaci/X_b.csv").to_numpy()
Y = pd.read_csv("Podaci/y_b.csv").to_numpy().reshape(-1,)
random=np.random.choice(2000,1000,replace=False)
#X=X[random]
#Y=Y[random]

# fit the model
clf = svm.SVC(kernel='linear', C=100)
clf.fit(X, Y)

# get the separating hyperplane
w = clf.coef_[0]
print("w0: ",w[0],"w1: ",w[1])
#print(w)
k = -w[0] / w[1]
xx = np.linspace(-10, 30)#default len is 50 #za a,b podatke (-10,30),za c podatke (-5,15)
yy = k * xx - (clf.intercept_[0]) / w[1]#0=w0x+w1y => y=-w0x/w1 =>k=-w0x/w1

# plot the parallels to the separating hyperplane that pass through the
# support vectors
bdown = clf.support_vectors_[0]
#print(clf.support_vectors_)
yy_down = k * xx + (bdown[1] - k * bdown[0])
bup = clf.support_vectors_[-1]
yy_up = k * xx + (bup[1] - k * bup[0])
margin=(bup[1] - k * bup[0])-(bdown[1] - k * bdown[0])
print("margina: ",margin)

# plot the line, the points, and the nearest vectors to the plane
pl.set_cmap(pl.cm.Paired)
pl.plot(xx, yy,"black")
pl.plot(xx, yy_down, 'k--')
pl.plot(xx, yy_up, 'k--')

pl.scatter(clf.support_vectors_[:, 0], clf.support_vectors_[:, 1],
           s=80, facecolors='none')
pl.scatter(X[:, 0], X[:, 1], c=Y)

#pl.axis('tight')
pl.show()
#c---------------------------------------------------------------------------
X = pd.read_csv("Podaci/X_c.csv").to_numpy()
Y = pd.read_csv("Podaci/y_c.csv").to_numpy().reshape(-1,)
random=np.random.choice(2000,1000,replace=False)
#X=X[random]
#Y=Y[random]

# fit the model
clf = svm.SVC(kernel='linear', C=100)
clf.fit(X, Y)

# get the separating hyperplane
w = clf.coef_[0]
print("w0: ",w[0],"w1: ",w[1])
#print(w)
k = -w[0] / w[1]
xx = np.linspace(-5, 15)#default len is 50 #za a,b podatke (-10,30),za c podatke (-5,15)
yy = k * xx - (clf.intercept_[0]) / w[1]#0=w0x+w1y => y=-w0x/w1 =>k=-w0x/w1

# plot the parallels to the separating hyperplane that pass through the
# support vectors
bdown = clf.support_vectors_[0]
#print(clf.support_vectors_)
yy_down = k * xx + (bdown[1] - k * bdown[0])
bup = clf.support_vectors_[9]#za -1 nije dobro ispalo pa sam povecao dok nije izgledalo dobro
yy_up = k * xx + (bup[1] - k * bup[0])
margin=(bup[1] - k * bup[0])-(bdown[1] - k * bdown[0])
print("margina: ",margin)

# plot the line, the points, and the nearest vectors to the plane
pl.set_cmap(pl.cm.Paired)
pl.plot(xx, yy,"black")
pl.plot(xx, yy_down, 'k--')
pl.plot(xx, yy_up, 'k--')

pl.scatter(clf.support_vectors_[:, 0], clf.support_vectors_[:, 1],
           s=80, facecolors='none')
pl.scatter(X[:, 0], X[:, 1], c=Y)

#pl.axis('tight')
pl.show()