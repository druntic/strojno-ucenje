# -*- coding: utf-8 -*-
"""
Created on Sun Mar 15 13:56:48 2020

@author: David
"""


import numpy as np
import pylab as pl
from sklearn import svm


np.random.seed(0)
X = np.array([[2,3.5], [2,3], [1,4], [3,3], [5,2], [4,2.25], [6,3]])
Y = np.array([1, 1, 1, 1, -1, -1, -1])
print(X,Y)



# fit the model
clf = svm.SVC(kernel='linear', C=100)
clf.fit(X, Y)

# get the separating hyperplane
w = clf.coef_[0]
print("w0: ",w[0],"w1: ",w[1])
#print(w)
k = -w[0] / w[1]
xx = np.linspace(-5, 5)#default len is 50
yy = k * xx - (clf.intercept_[0]) / w[1]#0=w0x+w1y => y=-w0x/w1 =>k=-w0x/w1

# plot the parallels to the separating hyperplane that pass through the
# support vectors
bdown = clf.support_vectors_[0]
print(bdown)
yy_down = k * xx + (bdown[1] - k * bdown[0])
bup = clf.support_vectors_[-1]
yy_up = k * xx + (bup[1] - k * bup[0])
margin=(bup[1] - k * bup[0])-(bdown[1] - k * bdown[0])
print(margin)

# plot the line, the points, and the nearest vectors to the plane
pl.set_cmap(pl.cm.Paired)
pl.plot(xx, yy,"black")
pl.plot(xx, yy_down, 'k--')
pl.plot(xx, yy_up, 'k--')

pl.scatter(clf.support_vectors_[:, 0], clf.support_vectors_[:, 1],
           s=80, facecolors='none')
pl.scatter(X[:, 0], X[:, 1], c=Y)

#pl.axis('tight')
pl.show()