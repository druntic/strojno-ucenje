#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
import pylab as pl
from sklearn import svm

# ### 1. Upoznavanje s SVM implementacijom iz sklearn-a

# Zadan vam je skup podataka $X$ koji ima $m=7$ primjera i pripadne oznake $y$. Koristeći scatter_plot vizualizirajte podatke i označite pripadne klase. <br>
# 
# - Primijenite model `SVC` s linearnom funkcijom na  (https://scikit-learn.org/stable/modules/generated/sklearn.svm.SVC.html) <br>
# - Ispišite koeficijente $\theta_0$ i $\theta$. 
# <br>
# - Na scatter_plot-u s početka zadatke nacrtajte pravac određen koeficijentima $\theta_0$ i $\theta$
# - Ispišite potporne vektore. Na gornjem scatter_plotu označite potporne vektore. 
# - Izračunajte širinu dobivene margine.
# 
# Uočite koji su potporni vektori i kolika je njihova udaljetnost do hiperravnine. 

# In[2]:


from sklearn.svm import SVC

X = np.array([[2,3.5], [2,3], [1,4], [3,3], [5,2], [4,2.25], [6,3]])
y = np.array([1, 1, 1, 1, -1, -1, -1])


# In[ ]:


# Your code here


# ### 2. Na podacima iz prošle zadaće 

# #### Podaci a)

# In[6]:


data_path = ''

X2 = pd.read_csv(data_path+'X_a.csv').to_numpy()
y2 = pd.read_csv(data_path+'y_a.csv').to_numpy().reshape(-1,)


# In[ ]:


# Your code here


# #### Podaci b)

# In[8]:


data_path = ''

X2_b = pd.read_csv(data_path+'X_b.csv').to_numpy()
y2_b = pd.read_csv(data_path+'y_b.csv').to_numpy().reshape(-1,)


# In[ ]:


# Your code here


# #### Podaci c)

# In[ ]:


data_path = ''

X2_c = pd.read_csv(data_path+'X_b.csv').to_numpy()
y2_c = pd.read_csv(data_path+'y_b.csv').to_numpy().reshape(-1,)


# In[ ]:


# Your code here


# #### Što možete zaključiti o udaljenosti najbližeg podatka do margine u ovom slučaju u usporedbi sa onim iz prošle zadaće?

# In[ ]:


# Your answer here


# ### SVM na dijelu podataka

# In[10]:


random_sample = np.random.choice(np.arange(0,X2_b.shape[0]),500)


# In[ ]:


# Your code here


# Što možete zakključiti o ulozi potpornih vektora?

# In[2]:


# 


# # 3. Logistička regresija

# In[15]:


def h(z):
    #Your code here

def gradientMethod(X,Y,alpha, numIter): 
    # Your code here
    return theta


# In[ ]:


data_path = # your path here '../heart.csv'
data = pd.read_csv(data_path)


df = pd.DataFrame(data)
df = df.dropna()
df.head()


# In[17]:


# Broj podataka 0/1
print('CHD in ten years not predicted = ', np.sum(df.apply(lambda x: True if x['TenYearCHD'] == 0 else False , axis=1)))
print('CHD in ten years predicted =  ', df.count()[0]- np.sum(df.apply(lambda x: True if x['TenYearCHD'] == 0 else False , axis=1)))


# In[20]:


# Koje feature zelimo modelirati:
features = df[['male', 'age','cigsPerDay', 'totChol', 'sysBP','glucose', 'TenYearCHD']]


# In[ ]:


# Your code here


# Napravimo predikcije za testni skup

# In[ ]:


# Your code here


#  Analizirajmo dobivene rezultate

# In[ ]:


# Your code here


# 
