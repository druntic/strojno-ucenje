# -*- coding: utf-8 -*-
"""
Created on Mon Mar 16 17:53:24 2020

@author: David
"""

import numpy as np
import pylab as pl
from sklearn import svm
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import scale
import matplotlib.pyplot as plt
heart = pd.read_csv("Podaci/heart.csv")
heart["age"]=scale(heart["age"])
heart["heartRate"]=scale(heart["heartRate"])
heart["glucose"]=scale(heart["glucose"])
heart["cigsPerDay"]=scale(heart["cigsPerDay"])
heart["totChol"]=scale(heart["totChol"])
heart["sysBP"]=scale(heart["sysBP"])
heart["diaBP"]=scale(heart["diaBP"])
heart["BMI"]=scale(heart["BMI"])
df = pd.DataFrame(heart)
df = df.dropna()
df.head()
y=heart["TenYearCHD"]

print(y)

X_train, X_test, y_train, y_test = train_test_split(heart,y)
print(X_test,y_test)



#y = lambda x : m*x + b
def summation(y,x_points, y_points):
    total1 = 0
    total2 = 0
    for i in range(1, len(x_points)):
        total1 += y(x_points[i]) - y_points[i]
        total2 += (y(x_points[i]) - y_points[i]) * x_points[i]        
    return total1/len(y_points), total2/len(y_points)

def gradient_descent(x_points,y_points,alpha,n):
    m=b=0
    y = lambda x : m*x + b
    for i in range(n):
        s1, s2 = summation(y, x_points, y_points)
        m = m - alpha * s2
        b = b - alpha * s1
    return m,b

print(gradient_descent(heart["male"],y,0.1,5))
print(gradient_descent(heart["age"],y,0.1,5))
