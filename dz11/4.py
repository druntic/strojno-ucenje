# -*- coding: utf-8 -*-
"""
Created on Fri May 22 02:09:41 2020

@author: David
"""
from sklearn import preprocessing
from scipy.stats import multivariate_normal as mvn
import numpy as np
import pandas as pd
import torch
import matplotlib.pyplot as plt
import scipy.stats as stats
import sklearn
from scipy.stats import multivariate_normal
from sklearn import mixture
from numpy.core.umath_tests import matrix_multiply as mm


#source code:https://people.duke.edu/~ccc14/sta-663/EMAlgorithm.html
def em_gmm_vect(xs, pis, mus, sigmas, tol=0.01, max_iter=100):

    n, p = xs.shape
    k = len(pis)

    ll_old = 0
    ll=[]
    itr=0
    for i in range(max_iter):
        exp_A = []
        exp_B = []
        ll_new = 0
        
        # E-step
        ws = np.zeros((k, n))
        for j in range(k):
            ws[j, :] = pis[j] * mvn(mus[j], sigmas[j]).pdf(xs)
        ws /= ws.sum(0)

        # M-step
        pis = ws.sum(axis=1)
        pis /= n

        mus = np.dot(ws, xs)
        mus /= ws.sum(1)[:, None]

        sigmas = np.zeros((k, p, p))
        for j in range(k):
            ys = xs - mus[j, :]
            sigmas[j] = (ws[j,:,None,None] * mm(ys[:,:,None], ys[:,None,:])).sum(axis=0)
        sigmas /= ws.sum(axis=1)[:,None,None]

        # update complete log likelihoood
        ll_new = 0
        for pi, mu, sigma in zip(pis, mus, sigmas):
            ll_new += pi*mvn(mu, sigma).pdf(xs)
        ll_new = np.log(ll_new).sum()

        if np.abs(ll_new - ll_old) < tol:
            break
        ll_old = ll_new
        
        itr+=1
        ll.append(ll_old)
    #print("itr: ",itr)
    return ll, pis, mus, sigmas,itr

# create data set
xs = np.array(np.load('Podaci/z2-data.npy'))
xs = preprocessing.scale(xs)
# initial guesses for parameters
for i in range (2,6):
    
    k=i
    print("k:",k)
    pis = np.random.random(k)
    pis /= pis.sum()
    mus = np.random.random((k,k))
    sigmas = np.random.random(k)
    
    ll1, pis1, mus1, sigmas1, itr = em_gmm_vect(xs, pis, mus, sigmas)    
    for j in range (9):
        ll2, pis2, mus2, sigmas2,itr = em_gmm_vect(xs, pis, mus, sigmas)
        if ll2[-1]>ll1[-1]:
            ll1=ll2
            pis1=pis2
            mus1=mus2
            sigmas1=sigmas2

    print("itr:", itr)
    print(ll1[-1])
    plt.scatter(np.arange(1,len(ll1)+1),ll1, s=150)
    plt.show()