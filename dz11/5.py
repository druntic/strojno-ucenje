# -*- coding: utf-8 -*-
"""
Created on Thu May 21 23:15:21 2020

@author: David
"""

import csv
import numpy as np
import pandas as pd
import torch
import matplotlib.pyplot as plt
import scipy.stats as stats
import sklearn
from scipy.stats import multivariate_normal
from sklearn import mixture
#napomena: u csv file treba zamijeniti svaki " " s ","
X = pd.read_csv("Podaci/djelatnici.csv",';',decimal = ',').to_numpy()
print(X)
print("kmeans,random")
for i in range (2,11):
    model1 = sklearn.mixture.GaussianMixture(n_components=i, tol=0.001, max_iter=100, n_init=1, init_params='kmeans')
    model1.fit(X)
    model2 = sklearn.mixture.GaussianMixture(n_components=i, tol=0.001, max_iter=100, n_init=1, init_params='random')
    model2.fit(X)
    
    print(i,model1.n_iter_,model2.n_iter_)
#izgleda da ce kmeans prije konvergirati