# -*- coding: utf-8 -*-
"""
Created on Thu May 21 23:05:02 2020

@author: David
"""
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import style
from scipy.spatial.distance import cdist


def plot_kmeans(X,labels, centers, n_clusters=4, rseed=0, ax=None):
    # Proslijedite argumente
    # X- podaci
    #labels - oznake pripadnosi klasteru za svaki podatak
    #centers - centri klastera
    #n_cluster - broj klastera
    
    
    # plot the input data
    ax = ax or plt.gca()
    ax.axis('equal')
    ax.scatter(X[:, 0], X[:, 1], c=labels, s=40, cmap='viridis', zorder=2)

    # plot the representation of the KMeans model
    radii = [cdist(X[labels == i], [center]).max()
             for i, center in enumerate(centers)]
    for c, r in zip(centers, radii):
        ax.add_patch(plt.Circle(c, r, fc='#CCCCCC', lw=3, alpha=0.5, zorder=1))
        
X = np.array(np.load('Podaci/z1-data1.npy'))
kmeans = KMeans(n_clusters=5).fit(X)
plot_kmeans(X,kmeans.labels_,kmeans.cluster_centers_)
plt.show()
X = np.array(np.load('Podaci/z1-data2.npy'))
kmeans = KMeans(n_clusters=5).fit(X)
plot_kmeans(X,kmeans.labels_,kmeans.cluster_centers_)
plt.show()