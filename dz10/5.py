# -*- coding: utf-8 -*-
"""
Created on Fri May 15 16:33:32 2020

@author: David
"""

# -*- coding: utf-8 -*-
"""
Created on Thu May 14 22:08:49 2020

@author: David
"""
from sklearn import metrics
from sklearn.metrics import calinski_harabasz_score
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import style
#source: https://medium.com/@rishit.dagli/build-k-means-from-scratch-in-python-e46bf68aa875
style.use('ggplot')
X = np.array(np.load('Podaci/z2-data.npy'))

plt.scatter(X[:,0], X[:,1], s=150)
plt.show()

class K_Means:
    
    def __init__(self, k=2, tol=0.001, max_iter=300):
        self.k = k
        self.tol = tol
        self.max_iter = max_iter
        
    def fit(self,data):

        self.centroids = {}
        iter=0
        self.Fnlist=Fnlist=[]
        for i in range(self.k):
            self.centroids[i] = data[i]

        for i in range(self.max_iter):
            print("iter: ",iter)
            iter=iter+1
            self.classifications = {}

            for i in range(self.k):
                self.classifications[i] = []

            for featureset in data:
                distances = [(np.linalg.norm(featureset-self.centroids[centroid]))**2 for centroid in self.centroids]
                classification = distances.index(min(distances))
                self.classifications[classification].append(featureset)
               
            sum=0
            for i in range(self.k):
                sumc=0
                #print(i)
                for x in self.classifications[i]:
                    sumc=sumc+(np.linalg.norm(x-self.centroids[i]))**2
                    #print(x)
                    #print((np.linalg.norm(x-self.centroids[i]))**2)
                #print("centroid",self.centroids[i])
                sum=sum+sumc
            print("Fn: ",sum)
            Fnlist.append(sum)
            #print(Fnlist)
                
                    
                    
            prev_centroids = dict(self.centroids)

            for classification in self.classifications:
                self.centroids[classification] = np.average(self.classifications[classification],axis=0)

            optimized = True

            for c in self.centroids:
                original_centroid = prev_centroids[c]
                current_centroid = self.centroids[c]
                if np.sum((current_centroid-original_centroid)/original_centroid*100.0) > self.tol:
                    #print("Fls: ",np.sum((current_centroid-original_centroid)/original_centroid*100.0))
                    
                    optimized = False

            if optimized:
                break

    def predict(self,data):
        distances = [np.linalg.norm(data-self.centroids[centroid]) for centroid in self.centroids]
        classification = distances.index(min(distances))
        return classification
    

Fnfinal=[]
for i in range(2,11):
    print("k: ",i)
    model = K_Means(k=i)
    model.fit(X)
    for centroid in model.centroids:
        plt.scatter(model.centroids[centroid][0], model.centroids[centroid][1],
                    marker="o", color="k", s=150, linewidths=5)

    plt.scatter(X[:,0], X[:,1], s=150)
    colors = 10*["y", "g", "c", "b", "k","pink","r","m","gray"]
    for classification in model.classifications:
        color = colors[classification]
        for featureset in model.classifications[classification]:
            plt.scatter(featureset[0], featureset[1], marker="x", color=color, s=50, linewidths=5)
            
    plt.show()
    #print(model.Fnlist)
    plt.scatter(np.arange(1,len(model.Fnlist)+1),model.Fnlist, s=150)
    plt.show()
    Fnfinal.append(model.Fnlist[-1])
    labels=[]
    for j in X:
        labels.append(model.predict(j))
    #print(labels)
    print("calinski: ",metrics.calinski_harabasz_score(X, labels))
    print("silhouette: ",metrics.silhouette_score(X, labels, metric='euclidean'))
plt.scatter(np.arange(1,len(Fnfinal)+1),Fnfinal, s=150)
plt.show()
#najveci calinski jer za k=10 pa je to najbolji odabir
