# -*- coding: utf-8 -*-
"""
Created on Sun Mar 29 16:38:16 2020

@author: David
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import math
from sklearn.datasets import load_boston
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import scale

from sklearn.metrics import confusion_matrix, recall_score, precision_score, accuracy_score
from sklearn.datasets import load_breast_cancer


data = load_breast_cancer()
x_train, x_test ,y_train ,y_test = train_test_split(data.data,data.target,test_size=.10,random_state=5)
x_train = scale(x_train)
x_test = scale(x_test)


def plot(A):
    plt.plot(np.arange(A.shape[0]), A, color='orchid')
    plt.title('Promjena funkcije pogreške kroz iteracije')
    plt.show()

def gradientMethod(X,Y,alpha=0.001, numIter=1000): 
    # Argumenti: X - matrica dizajna, Y-vektor izlaza, alpha - stopa ucenja, numIter - broj iteracija
    m = X.shape[0] 
    n = X.shape[1] 
    theta =  np.zeros((n,1))
    cost = np.zeros(numIter) 
    for i in range(numIter):
        XThetaY = X.dot(theta)-Y 
        gradJ = (X.T.dot(XThetaY))/m
        theta = theta - alpha*gradJ 
        
        cost[i] = (XThetaY.T).dot(XThetaY)/(2*m)
    return theta, cost
def stochasticGradientMethod(X,Y,alpha=0.001, numIter=1000): 
    # Argumenti: X - matrica dizajna, Y-vektor izlaza, alpha - stopa ucenja, numIter - broj iteracija
    m = X.shape[0] 
    n = X.shape[1] 
    theta =  np.zeros((n,1))
    cost = np.zeros(numIter)
    for itera in range(numIter):
        sample = np.random.randint(m)
        X = X[sample].reshape(1,-1) 
        Y = Y[sample].reshape(-1,1) 
        m = X.shape[0]
        XThetaY = (X.dot(theta)-Y).reshape(-1,1) 
        gradJ = (X.T.dot(XThetaY))/m 
        theta = theta - alpha*gradJ 
        
        cost[itera] = (XThetaY.T).dot(XThetaY)/(2*m) 
    return theta, cost
def h(z):
    return (1.0 / (1.0 + np.exp(-z)))


theta, cost = stochasticGradientMethod(np.column_stack((np.ones((x_train.shape[0],1)),x_train)),y_train.reshape(-1,1), numIter=5000)
plot(cost)
#print(cost[-1])
#print(theta)
#potrebno je par puta upaliti skritpu jer zbog nekog razlika ne radi uvijek kako treba
"""from sklearn.linear_model import LogisticRegression
log_reg = LogisticRegression(max_iter=200).fit(x_train,y_train)

#print(log_reg.coef_)

y_test_pred = log_reg.predict(x_test)

tn, fp, fn, tp = confusion_matrix(y_test, y_test_pred).ravel()
print('Confusion matrix = tn, fp, fn, tp = ', tn, fp, fn, tp)

print('accuracy = ', accuracy_score(y_test, y_test_pred))
print('precision = ', precision_score(y_test, y_test_pred))
print('recall = ', recall_score(y_test, y_test_pred))"""
# Trening set
n = x_train.shape[1]
X1 = np.ones((x_train.shape[0],n+1))
X1[:,1:] = scale(x_train)
y1 = y_train.reshape(-1,1)

n = x_train.shape[1]
X1 = np.ones((x_train.shape[0],n+1))
X1[:,1:] = scale(x_train)
y1 = y_train.reshape(-1,1)


# Test set
X1_test = np.ones((x_test.shape[0],n+1))
X1_test[:,1:] = scale(x_test)
y1_test = y_test.reshape(-1,1)

y1_test_pred = h(X1_test.dot(theta)).reshape(-1,1)
y1_test_pred = np.array([1 if i >= 0.5 else -1 for i in y1_test_pred ]).reshape(-1,1)


theta,cost=stochasticGradientMethod(X1, y1,0.001, 2000)
print(theta)
plot(cost)
y1_test_pred = h(X1_test.dot(theta)).reshape(-1,1)
y1_test_pred = np.array([1 if i >= 0.5 else -1 for i in y1_test_pred ]).reshape(-1,1)

y1_test = y1_test.reshape(-1,1)
tn, fp, fn, tp = confusion_matrix(y1_test, y1_test_pred).ravel()
print('Confusion matrix = tn, fp, fn, tp = ', tn, fp, fn, tp)

print('accuracy = ', accuracy_score(y_test, y1_test_pred))
print('precision = ', precision_score(y_test, y1_test_pred))
print('recall = ', recall_score(y_test, y1_test_pred))
