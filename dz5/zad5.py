# -*- coding: utf-8 -*-
"""
Created on Mon Mar 30 13:52:20 2020

@author: David
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
#matplotlib inline
import scipy.sparse
from sklearn.datasets import load_digits
import sklearn.datasets
from sklearn.model_selection import train_test_split
import math
digits = load_digits()



X,y = sklearn.datasets.load_digits(return_X_y=True)
x_train,x_test,y_train,y_test=train_test_split(X,y, test_size=.30,random_state=5)
print(len(x_train),len(x_test))
x_train1=np.array(x_train)
y_train1=np.array(y_train)
print(len(X[1]))
"""def softMax(x):
    x#vektor
    v=[]*len(x)
    sum=0
    for i in range(len(x)):
        sum=sum+math.exp(x[i])
        #print(sum)
    for i in range(len(x)):
        #print("i: ", i)
        v.append(math.exp(x[i])/sum)
        
    return v

x=[1,2,3]

print(softMax(x))"""
#source code: google softmax regression python first link
print(y)
#print(digits)
def getLoss(w,x,y,lam):
    m = x.shape[0] #First we get the number of training examples
    y_mat = oneHotIt(y) #Next we convert the integer class coding into a one-hot representation
    scores = np.dot(x,w) #Then we compute raw class scores given our input and current weights
    #print(np.shape(scores))
    prob = softmax(scores) #Next we perform a softmax on these scores to get their probabilities
    #print(np.shape(y_mat))
    #print(np.shape((prob)))
    loss = (-1 / m) * np.sum(y_mat * np.log(prob)) + (lam/2)*np.sum(w*w) #We then find the loss of the probabilities
    grad = (-1 / m) * np.dot(x.T,(y_mat - prob)) + lam*w #And compute the gradient for that loss
    return loss,grad
def oneHotIt(Y):
    m = Y.shape[0]
    #Y = Y[:,0]
    OHX = scipy.sparse.csr_matrix((np.ones(m), (Y, np.array(range(m)))))
    OHX = np.array(OHX.todense()).T
    return OHX
def softmax(z):
    z -= np.max(z)
    sm = (np.exp(z).T / np.sum(np.exp(z),axis=1)).T
    return sm
def getProbsAndPreds(someX):
    probs = softmax(np.dot(someX,w))
    preds = np.argmax(probs,axis=1)
    return probs,preds
w = np.zeros([x_train1.shape[1],len(np.unique(y_train1))])
lam = 1
iterations = 2000
learningRate = 0.001
losses = []
for i in range(0,iterations):
    loss,grad = getLoss(w,x_train1,y_train1,lam)
    losses.append(loss)
    w = w - (learningRate * grad)
print (loss)
plt.plot(losses)