# -*- coding: utf-8 -*-
"""
Created on Tue Mar 31 18:17:38 2020

@author: David
"""


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
#matplotlib inline
import scipy.sparse
from sklearn.datasets import load_digits
import sklearn.datasets
from sklearn.model_selection import train_test_split
import math
data_path = 'Podaci\winequality-white.csv'
df = pd.read_csv(data_path)
df = df.dropna()
df.head()
#print(df["pH"])


features = df[['fixed acidity', 'volatile acidity','citric acid', 'residual sugar', 'chlorides','free sulfur dioxide', 'total sulfur dioxide',"density","pH","sulphates","alcohol","quality"]]
#print(y)

X = features.iloc[:,:-1]
y = features.iloc[:,-1]
x_train,x_test,y_train,y_test=train_test_split(X,y, test_size=.30,random_state=5)
#print(len(x_train),len(x_test))
x_train1=np.array(x_train)
y_train1=np.array(y_train)
#print(np.shape(X))
#source code: google softmax regression python first link
def getLoss(w,x,y,lam):
    m = x.shape[0] #First we get the number of training examples
    y_mat = oneHotIt(y) #Next we convert the integer class coding into a one-hot representation
    scores = np.dot(x,w) #Then we compute raw class scores given our input and current weights
    #print(np.shape(w))
    #print(np.shape(scores))
    prob = softmax(scores) #Next we perform a softmax on these scores to get their probabilities
    #print(np.shape(y_mat))
    #print(np.shape((prob)))
    #print(np.log(prob))
    loss = (-1 / m) * np.sum(y_mat * np.log(prob)) + (lam/2)*np.sum(w*w) #We then find the loss of the probabilities
    grad = (-1 / m) * np.dot(x.T,(y_mat - prob)) + lam*w #And compute the gradient for that loss
    return loss,grad
def oneHotIt(Y):
    m = Y.shape[0]
    #Y = Y[:,0]
    OHX = scipy.sparse.csr_matrix((np.ones(m), (Y, np.array(range(m)))))
    OHX = np.array(OHX.todense()).T
    return OHX
def softmax(z):
    z -= np.max(z)
    sm = (np.exp(z).T / np.sum(np.exp(z),axis=1)).T
    return sm
def getProbsAndPreds(someX):
    probs = softmax(np.dot(someX,w))
    preds = np.argmax(probs,axis=1)
    return probs,preds
w = np.zeros([x_train1.shape[1],len(np.unique(y_train1))+3])#zbog nekog razloga izbacivalo je error jer je razlika u dimenzijama bila 3
#print(len(np.unique(y_train1)))
#print(y_train1)
lam = 1
iterations = 2000
learningRate = 0.00001
losses = []
for i in range(0,iterations):
    loss,grad = getLoss(w,x_train1,y_train1,lam)
    losses.append(loss)
    w = w - (learningRate * grad)
print (loss)
plt.plot(losses)

def h(z):
    return (1.0 / (1.0 + np.exp(-z)))
#y1_test_pred = h(x_train1.dot(w)).reshape(-1,1)

#print(len(y1_test_pred))
from sklearn.metrics import accuracy_score
#accuracy_score(y1_test_pred,y_train1)