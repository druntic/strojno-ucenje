# -*- coding: utf-8 -*-
"""
Created on Mon Apr 20 19:33:03 2020

@author: David
"""
import numpy as np
import pandas as pd
import torch

x = np.array([[3.3], [4.4], [5.5], [6.71], [6.93], [4.168], [9.779], [6.182], [7.59], [2.167], [7.042], [10.791], [5.313], [7.998], [3.1]], dtype=np.float32)
y = np.array([[1.7], [2.76], [2.09], [3.19], [2.094], [1.57], [3.366], [2.59], [2.82], [1.221], [2.821], [3.456], [1.65], [2.99], [1.3]], dtype=np.float32)
w1 = np.load('Podaci/w1.npy')
w2 = np.load('Podaci/w2.npy')


def sigmoid(x):
        return 1.0/(1.0+np.exp(-x))
def forwardprop(x,y):
    a2=x.dot(w1)
    z2=sigmoid(a2)
    a3=z2.dot(w2)
    
    return a3

a3=forwardprop(x,y)
loss=np.square(a3-y).sum()
print(loss)