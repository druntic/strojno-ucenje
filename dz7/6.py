# -*- coding: utf-8 -*-
"""
Created on Wed Apr 22 17:06:54 2020

@author: David
"""

import numpy as np
import pandas as pd
import torch
import matplotlib.pyplot as plt
x = pd.read_csv('Podaci/data-x.csv')
y = pd.read_csv('Podaci/data-y.csv')

x = torch.tensor(np.array(x), dtype = torch.float)#x = np.array(x)
y = torch.tensor(np.array(y), dtype = torch.float)
#print(x[0])
#print(list(x.size()))

inputSize = 5
hiddenSize = 50
outputSize = 1

# Postavimo početne težine na neke slučajno generirane brojeve
w1 = torch.randn(inputSize, hiddenSize, dtype = torch.float, requires_grad = True)
w2 = torch.randn(hiddenSize, outputSize, dtype = torch.float, requires_grad = True)
learningRate = 1e-6

model = torch.nn.Sequential(
        torch.nn.Linear(inputSize, hiddenSize),
        torch.nn.Sigmoid(),
        torch.nn.Linear(hiddenSize, outputSize))


loss_fn = torch.nn.MSELoss()
loss_values = []


for i in range(500):
    # Prolazak unaprijed (forward pass): izračunaj predikciju
    # Sada je potrebno samo ovo
    y_pred = model(x)
    
    
    # Nakon sto smo napravili jedan prolazak unaprijed idemo izracunati pogresku
    loss = loss_fn(y_pred,y )
    loss_values.append(loss.item())
    if i%100==0:
        print ('Pogreska u iteraciji {} iznosi {}'.format(i, loss.item()))
    
    # Sada ćemo napraviti backward propagaciju, izračunati gradijente
    # Prvo ćemo ih sve postaviti na 0
    model.zero_grad()
    # Poziv funkcije
    loss.backward()
    
    # Sada ćemo ažurirati težine w
    with torch.no_grad():
        # Proći ćemo kroz sve težine u našem modelu
        for w in model.parameters():
            w -= learningRate*w.grad
            
plt.plot(np.arange(0,500,1), loss_values)
plt.show()