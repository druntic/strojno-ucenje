# -*- coding: utf-8 -*-
"""
Created on Sat Mar  7 23:27:15 2020

@author: David
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import scale
from sklearn import linear_model


Xa = pd.read_csv (r'podaci/X_a.csv')
target=pd.read_csv (r'podaci/y_a.csv')
Xa=Xa.dropna()
target=target.dropna()


x=scale(Xa["x"])
y=scale(Xa["y"])
n = sum(1 for row in Xa)
m=len(x)
for i in range (len(x)):
    plt.scatter(x[i],y[i])#prvih 1000 podataka su gore a zadnjih 1000 su dolje
plt.show()

z=[]
for i in range(len(x)):
    z.append([1,x[i],y[i]])
z=np.matrix(z)

print(z.item(0,2))#z.shape[0]=m
d=[]
for i in range(np.size(z,1)):
    d.append(z.item(0,i))
print(d)
    
print("--------")

#print(z)    
theta=[0]*n
#print(target["y"][0])

def perceptron(X,y):
    iterations=10
    alpha=0.001
    n=np.size(X,1)-1
    m=np.size(X,0)
    w=[0.0]*n
    bias=1
    for we in range(iterations):  
        for i in range(1,m):#prvi element je 1
            sum=0
            for j in range(1,n):
                sum=sum+X.item(i,j)*w[j]
    
            if bias+sum>=0:
                h=1
            else:
                h=-1
            if h!=y[i]:
               for j in range(n):
                   w[j]=w[j]+(alpha*y[i]*X.item(i,j))
                   bias=bias+alpha*y[j]                             
        print(w)

    plt.plot(x, x*w[0]+w[1], color="blue" )
           
   
perceptron(z,target["y"])
for i in range (len(x)):
    plt.scatter(x[i],y[i])#prvih 1000 podataka su gore a zadnjih 1000 su dolje
plt.show()       

    


    