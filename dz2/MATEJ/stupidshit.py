# -*- coding: utf-8 -*-
"""
Created on Sun Mar  8 15:18:59 2020

@author: David
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import scale
from sklearn import linear_model


Xa = pd.read_csv (r'podaci/X_a.csv')
target=pd.read_csv (r'podaci/y_a.csv')
Xa=Xa.dropna()
target=target.dropna()