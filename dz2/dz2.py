# -*- coding: utf-8 -*-
"""
Created on Sun Mar  8 15:09:11 2020

@author: David
"""


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import scale
from sklearn import linear_model


Xa = pd.read_csv (r'podaci/X_a.csv')
target=pd.read_csv (r'podaci/y_a.csv')
Xa=Xa.dropna()
target=target.dropna()

Xb = pd.read_csv (r'podaci/X_b.csv')
targetb=pd.read_csv (r'podaci/y_b.csv')
Xb=Xb.dropna()
targetb=targetb.dropna()


x=(Xa["x"])
y=(Xa["y"])
n = sum(1 for row in Xa)
m=len(x)
for i in range (len(x)):
    plt.scatter(x[i],y[i])#prvih 1000 podataka su gore a zadnjih 1000 su dolje
plt.show()

X=[]
for i in range(m):
    X.append([1,x[i],y[i]])
X=np.matrix(X)
#print(X)


xb=(Xb["x"])
yb=(Xb["y"])
mb=len(xb)
for i in range (mb):
    plt.scatter(xb[i],yb[i])
plt.show()

Xb=[]
for i in range(m):
    Xb.append([1,xb[i],yb[i]])
Xb=np.matrix(Xb)
#print(Xb)

#a
def perceptron(X,y):
    n=np.size(X,1)
    m=np.size(X,0)
    w=[0]*n
    for k in range(50):
        go=True
        for i in range(m):
            sum=0
            for j in range(1,n):
                sum=sum+X.item(i,j)*w[j]
            if sum>=0:
                h=1
            else:
                h=-1
            if y[i]!=h:
                for j in range(n):
                    X.itemset((i,j),X.item(i,j)*y[i])
                for j in range(n):
                    w[j]=w[j]+X.item(i,j)
                """w+=y[i]*X[i]
                go=False"""
            
                    
    #0=w0+w1x+w2y=> y=-(w0+w1x)/w2
    
    return w
#b i d

wa=perceptron(X,target["y"])
plt.plot(x, -(wa[0]+wa[1]*x)/wa[2], color="blue" )            
for i in range (m):
    if(i<=m/2):
        plt.scatter(x[i],y[i], color="red")
    else:
        plt.scatter(x[i],y[i], color="green")
plt.show()
print("w1: ",wa)
wb=perceptron(Xb,targetb["y"]) 
plt.plot(x, -(wb[0]+wb[1]*x)/wb[2], color="blue" )             
for i in range (mb):
    if(i<=mb/2):
        plt.scatter(xb[i],yb[i],color="green")
    else:
        plt.scatter(xb[i],yb[i], color="red")
plt.show()
print("w2: ",wb)
#c

#e
print("teze je naci w za prvi slucaj jer je manja razlika izmedu clustera")

#f
R1=0
for i in range(m):
    if R1<X.item(i,0)*X.item(i,0)+X.item(i,1)*X.item(i,1)+X.item(i,2)*X.item(i,2): #l2 norma
       R1=X.item(i,0)*X.item(i,0)+X.item(i,1)*X.item(i,1)+X.item(i,2)*X.item(i,2)
print("R1: ",R1)
R2=0
for i in range(mb):
    if R2<=Xb.item(i,0)*Xb.item(i,0)+Xb.item(i,1)*Xb.item(i,1)+Xb.item(i,2)*Xb.item(i,2):
       R2=Xb.item(i,0)*Xb.item(i,0)+Xb.item(i,1)*Xb.item(i,1)+Xb.item(i,2)*Xb.item(i,2)
print("R2: ",R2)

#2
#a
Xa2 = pd.read_csv (r'podaci/2X_a.csv')
target2=pd.read_csv (r'podaci/2y_a.csv')
Xa2=Xa2.dropna()
target2=target2.dropna()

xa2=Xa2["x"]
ya2=Xa2["y"]
for i in range (len(xa2)):
    
    if(i<=len(xa2)/2):
        plt.scatter(xa2[i],ya2[i], color="red")
    else:
        plt.scatter(xa2[i],ya2[i], color="green")
plt.show()
print("nekakva elipsa bi mogla razdvojiti ova 2 skupa")
X2=[]
for i in range(m):
    X2.append([1,xa2[i],ya2[i]])
X2=np.matrix(X2)
wa2=perceptron(X2,target2["y"])
print("wa2: ",wa2)
#b
Xb2 = pd.read_csv (r'podaci/2X_b.csv')
target2b=pd.read_csv (r'podaci/2y_b.csv')
Xb2=Xb2.dropna()
target2b=target2b.dropna()

xb2=Xb2["x"]
yb2=Xb2["y"]
for i in range (len(xb2)):
    
    if(i<=len(xb2)/2):
        plt.scatter(xb2[i],yb2[i], color="red")
    else:
        plt.scatter(xa2[i],ya2[i], color="green")
plt.show()
print("nekakva elipsa bi mogla razdvojiti ova 2 skupa")
X2b=[]
for i in range(m):
    X2b.append([1,xb2[i],yb2[i]])
X2b=np.matrix(X2b)
wb2=perceptron(X2b,target2b["y"])
print("ovo se nikako nece moci razdvojiti jer postoji presjek koji nije prazan skup")
print("wb2: ",wb2)

