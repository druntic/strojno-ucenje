# -*- coding: utf-8 -*-
"""
Created on Mon Apr 13 20:15:37 2020

@author: David
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import scale
from sklearn.model_selection import train_test_split 
from sklearn.metrics import confusion_matrix, recall_score, precision_score, accuracy_score


data_path = 'Podaci\winequality-white.csv'
df = pd.read_csv(data_path)
df = df.dropna()
df=df.replace({'white': 0, 'red': 1})
df.head()

features = df[['fixed acidity', 'volatile acidity','citric acid', 'residual sugar', 'chlorides','free sulfur dioxide', 'total sulfur dioxide',"density","pH","sulphates","alcohol","quality"]]
#print(y)

X = features.iloc[:,:-1].values

y = df["type"].values

x_train,x_test,y_train,y_test=train_test_split(X,y, test_size=.30,random_state=5)

print(x_train)


def sigmoid(z):
    """
    return the sigmoid of z
    """
    
    return 1/ (1 + np.exp(-z))

def costFunctionReg(theta, X, y ,Lambda):
    """
    Take in numpy array of theta, X, and y to return the regularize cost function and gradient
    of a logistic regression
    """
    
    m=len(y)
    y=y[:,np.newaxis]
    predictions = sigmoid(X @ theta)
    error = (-y * np.log(predictions)) - ((1-y)*np.log(1-predictions))
    cost = 1/m * sum(error)
    regCost= cost + Lambda/(2*m) * sum(theta**2)
    
    # compute gradient
    j_0= 1/m * (X.transpose() @ (predictions - y))[0]
    j_1 = 1/m * (X.transpose() @ (predictions - y))[1:] + (Lambda/m)* theta[1:]
    grad= np.vstack((j_0[:,np.newaxis],j_1))
    return cost[0], grad
initial_theta = np.zeros((X.shape[1], 1))
Lambda = 1
cost, grad=costFunctionReg(initial_theta, X, y, Lambda)

#print(X.shape)

def gradientDescent(X,y,theta,alpha,num_iters,Lambda):
    """
    Take in numpy array X, y and theta and update theta by taking num_iters gradient steps
    with learning rate of alpha
    
    return theta and the list of the cost of theta during each iteration
    """
    
    m=len(y)
    J_history =[]
    
    for i in range(num_iters):
        cost, grad = costFunctionReg(theta,X,y,Lambda)
        theta = theta - (alpha * grad)
        J_history.append(cost)
    
    return theta , J_history


for a in [0,10,100]:
    initial_theta = np.zeros((X.shape[1], 1))
    theta, cost = gradientDescent(x_train,y_train,initial_theta,alpha=0.01,num_iters=10000,Lambda=a)
    
    
