# -*- coding: utf-8 -*-
"""
Created on Sat Apr 11 22:06:59 2020

@author: David
"""

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits import mplot3d
from sklearn import linear_model


def costFunctionReg(X,y,theta,lamda ):
    '''Cost function for ridge regression (regularized L2)'''
    #Initialization
    m = len(y) 
    J = 0
    
    #Vectorized implementation
    h = X @ theta
    J_reg = (lamda / (2*m)) * np.sum(np.square(theta))
    J = float((1./(2*m)) * (h - y).T @ (h - y)) + J_reg;
    return(J) 


def gradient_descent_reg(X,y,theta,lamda ,alpha = 0.005,num_iters=100000):
    '''Gradient descent for ridge regression'''
    #Initialisation of useful values 
    m = np.size(y)
    J_history = np.zeros(num_iters)
    theta_0_hist, theta_1_hist = [], [] #Used for three D plot

    for i in range(num_iters):
        #Hypothesis function
        h = np.dot(X,theta)
        
        #Grad function in vectorized form
        theta = theta - alpha * (1/m)* (  (X.T @ (h-y)) + lamda * theta )
           
        #Cost function in vectorized form       
        J_history[i] = costFunctionReg(X,y,theta,lamda)
           
        #Calculate the cost for each iteration(used to plot convergence)
        theta_0_hist.append(theta[0,0])
        theta_1_hist.append(theta[1,0])   
    return theta ,J_history, theta_0_hist, theta_1_hist




x = pd.read_csv('./Podaci/DZ6-X_test_lin2.csv').to_numpy().reshape(-1,)
y = pd.read_csv('./Podaci/DZ6-y_test_lin2.csv').to_numpy()
x2 = pd.read_csv('./Podaci/DZ6-X_train_lin2.csv').to_numpy().reshape(-1,)
y2 = pd.read_csv('./Podaci/DZ6-y_train_lin2.csv').to_numpy()
#Design matrix is x, x^2
X = np.column_stack((x**0,x**1,x**2,x**3,x**4,x**5))
X2 = np.column_stack((x2**0,x2**1,x2**2,x2**3,x2**4,x2**5))
#print(X)
#Nornalizing the design matrix to facilitate visualization
#X = X / np.linalg.norm(X,axis = 0)
#print(X)
#Plotting the result
#plt.scatter(x,y, label = 'Dataset')
#plt.plot(x,y,label = 'Sine')

#plt.show()


theta1,Jhist1,theta0_hist,theta1_hist=gradient_descent_reg(X,y,np.array([0]*6).reshape(6,1),0)
theta2,Jhist2,theta0_hist,theta1_hist=gradient_descent_reg(X,y,np.array([0]*6).reshape(6,1),1)
theta3,Jhist3,theta0_hist,theta1_hist=gradient_descent_reg(X,y,np.array([0]*6).reshape(6,1),5)
thetatrain,Jhisttrain,theta0_histtrain,theta1_histtrain=gradient_descent_reg(X2,y2,np.array([0]*6).reshape(6,1),5)

def plotDecisionBoundary(X,y,theta):
    plt.scatter(X, y)
    xmin = np.min(X)
    xmax = np.max(X)
    xx = np.arange(xmin,xmax,0.1)
    XX = np.ones([xx.shape[0],6])
    XX[:,1] = xx
    XX[:,2] = xx**2
    XX[:,3] = xx**3
    XX[:,4] = xx**4
    XX[:,5] = xx**5
    yy = XX.dot(theta)
    plt.plot(xx,yy,'r')
    plt.show()

"""plotDecisionBoundary(x,y,theta1)
print(Jhist1[-1])
plotDecisionBoundary(x,y,theta2)
print(Jhist2[-1])
plotDecisionBoundary(x,y,theta3)
print(Jhist3[-1])# -*- coding: utf-8 -*-

plotDecisionBoundary(x2,y2,thetatrain)
print(Jhisttrain[-1])"""

jhistlist1=[0]*20
jhistlisttrain=[0]*20
counter=0
for i in range(0,210,10):
    
    theta1,Jhist1,theta0_hist,theta1_hist=gradient_descent_reg(X,y,np.array([0]*6).reshape(6,1),i)
    thetatrain,Jhisttrain,theta0_histtrain,theta1_histtrain=gradient_descent_reg(X2,y2,np.array([0]*6).reshape(6,1),i)
    print(Jhist1[-1])
    plotDecisionBoundary(x,y,theta1)
    print(Jhisttrain[-1])
    plotDecisionBoundary(x2,y2,thetatrain)
    print(i)
    jhistlist1[counter]=Jhist1[-1]
    jhistlisttrain[counter]=Jhisttrain[-1]
    counter=counter+1
    
print(jhistlisttrain)

"za lambda=0 imamo najmanju cost vrijednost"
"""
Created on Sun Apr 12 13:02:45 2020

@author: David
"""

