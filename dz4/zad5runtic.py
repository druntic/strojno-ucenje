# -*- coding: utf-8 -*-
"""
Created on Mon Mar 23 19:19:28 2020

@author: David
"""
import numpy as np
from sklearn.datasets import load_digits
import matplotlib.pyplot as plt 
import sklearn.datasets
from sklearn.model_selection import train_test_split
digits = load_digits()
print(digits.data.shape)
X,y = sklearn.datasets.load_digits(return_X_y=True)
x_train,x_test,y_train,y_test=train_test_split(X,y, test_size=.30,random_state=5)
#print(y_test)

plt.gray() 
plt.matshow(digits.images[9]) 
plt.show()
#print(digits.images[1])
#print(x_test[1]) 

#x_test[1]=np.reshape(x_test[0],(8,8))
#print(np.reshape(x_test[0],(8,8)))
plt.matshow(np.reshape(x_test[0],(8,8)))

for i in range(9):
    #plt.gray()
    plt.matshow(np.reshape(x_test[i],(8,8)))
    plt.show()
    print(y_test[i])