# -*- coding: utf-8 -*-
"""
Created on Mon Mar 23 15:08:24 2020

@author: David
"""

import numpy as np
import sklearn
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import random
from sklearn.svm import SVC
import sklearn.datasets

digits = sklearn.datasets.load_digits()
X,y = sklearn.datasets.load_digits(return_X_y=True)
x_train,x_test,y_train,y_test=train_test_split(X,y, test_size=.30,random_state=5)
print(x_test)
print(len(x_test[0]))#64=> 8x8, svakom podatku x_test[i] pridruzen je broj y_test[i] iz {0,..,9}
print(len(y_test))
#print(x_test[1][2])

plt.figure(5)
plt.gray()
