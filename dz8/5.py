# -*- coding: utf-8 -*-
"""
Created on Fri May  1 12:23:50 2020

@author: David
"""

import numpy as np
import pandas as pd
import torch
import matplotlib.pyplot as plt


def sigmoid(x):
    return 1.0/(1.0+np.exp(-x))

def forwardprop(x,y,w1,w2):
    a2=x.dot(w1)
    z2=sigmoid(a2)
    a3=z2.dot(w2)   
    return a3


def backwardprop(x,y,w1,w2):
    a3=forwardprop(x,y,w1,w2)
    d1 = (a3-y)
    alpha=0.001
    d2 = d1.dot(w2.T)*sigmoid(x)*(1-sigmoid(x))
    Dw2 = sigmoid(x).T.dot(d1)
    Dw1 = x.T.dot(d2)
    return w1 - (alpha)*Dw1, w2 - (alpha)*Dw2


x = np.load('Podaci/data-x.npy')
y = np.load('Podaci/data-y.npy')

x = torch.tensor(np.array(x), dtype = torch.float)#x = np.array(x)
y = torch.tensor(np.array(y), dtype = torch.float)


inputSize = 2
hiddenSize = 100
outputSize = 1

learningRate = 1e-6
#1

print("Sigmoid fixed")
w1=[[10 for x in range(inputSize)] for y in range(hiddenSize)] 
w2=[[10 for x in range(hiddenSize)] for y in range(outputSize)] 
w1 = torch.tensor(np.array(w1), dtype = torch.float,requires_grad = True)
w2 = torch.tensor(np.array(w2), dtype = torch.float,requires_grad = True)
learningRate = 1e-6

model = torch.nn.Sequential(
        torch.nn.Linear(inputSize, hiddenSize),
        torch.nn.Sigmoid(),
        torch.nn.Linear(hiddenSize, outputSize))


loss_fn = torch.nn.MSELoss()
loss_values = []


for i in range(50):
    # Prolazak unaprijed (forward pass): izračunaj predikciju
    # Sada je potrebno samo ovo
    y_pred = model(x)
    
    
    # Nakon sto smo napravili jedan prolazak unaprijed idemo izracunati pogresku
    loss = loss_fn(y_pred,y )
    loss_values.append(loss.item())
    if i%10==0:
        print ('Pogreska u iteraciji {} iznosi {}'.format(i, loss.item()))
    
    # Sada ćemo napraviti backward propagaciju, izračunati gradijente
    # Prvo ćemo ih sve postaviti na 0
    model.zero_grad()
    # Poziv funkcije
    loss.backward()
    
    # Sada ćemo ažurirati težine w
    with torch.no_grad():
        # Proći ćemo kroz sve težine u našem modelu
        for w in model.parameters():
            w -= learningRate*w.grad
            
plt.plot(np.arange(0,50,1), loss_values)
plt.show()
#2
print("Sigmoid rand")
w1 = torch.randn(inputSize, hiddenSize, dtype = torch.float, requires_grad = True)
w2 = torch.randn(hiddenSize, outputSize, dtype = torch.float, requires_grad = True)
learningRate = 1e-6

model = torch.nn.Sequential(
        torch.nn.Linear(inputSize, hiddenSize),
        torch.nn.Sigmoid(),
        torch.nn.Linear(hiddenSize, outputSize))


loss_fn = torch.nn.MSELoss()
loss_values = []


for i in range(50):
    # Prolazak unaprijed (forward pass): izračunaj predikciju
    # Sada je potrebno samo ovo
    y_pred = model(x)
    
    
    # Nakon sto smo napravili jedan prolazak unaprijed idemo izracunati pogresku
    loss = loss_fn(y_pred,y )
    loss_values.append(loss.item())
    if i%10==0:
        print ('Pogreska u iteraciji {} iznosi {}'.format(i, loss.item()))
    
    # Sada ćemo napraviti backward propagaciju, izračunati gradijente
    # Prvo ćemo ih sve postaviti na 0
    model.zero_grad()
    # Poziv funkcije
    loss.backward()
    
    # Sada ćemo ažurirati težine w
    with torch.no_grad():
        # Proći ćemo kroz sve težine u našem modelu
        for w in model.parameters():
            w -= learningRate*w.grad
            
plt.plot(np.arange(0,50,1), loss_values)
plt.show()

#3
print("ReLU fixed")
w1=[[0 for x in range(inputSize)] for y in range(hiddenSize)] 
w2=[[0 for x in range(hiddenSize)] for y in range(outputSize)] 
w1 = torch.tensor(np.array(w1), dtype = torch.float,requires_grad = True)
w2 = torch.tensor(np.array(w2), dtype = torch.float,requires_grad = True)
learningRate = 1e-6

model = torch.nn.Sequential(
        torch.nn.Linear(inputSize, hiddenSize),
        torch.nn.ReLU(),
        torch.nn.Linear(hiddenSize, outputSize))


loss_fn = torch.nn.MSELoss()
loss_values = []


for i in range(50):
    # Prolazak unaprijed (forward pass): izračunaj predikciju
    # Sada je potrebno samo ovo
    y_pred = model(x)
    
    
    # Nakon sto smo napravili jedan prolazak unaprijed idemo izracunati pogresku
    loss = loss_fn(y_pred,y )
    loss_values.append(loss.item())
    if i%10==0:
        print ('Pogreska u iteraciji {} iznosi {}'.format(i, loss.item()))
    
    # Sada ćemo napraviti backward propagaciju, izračunati gradijente
    # Prvo ćemo ih sve postaviti na 0
    model.zero_grad()
    # Poziv funkcije
    loss.backward()
    
    # Sada ćemo ažurirati težine w
    with torch.no_grad():
        # Proći ćemo kroz sve težine u našem modelu
        for w in model.parameters():
            w -= learningRate*w.grad
            
plt.plot(np.arange(0,50,1), loss_values)
plt.show()
#4
print("ReLU rand")
w1 = torch.randn(inputSize, hiddenSize, dtype = torch.float, requires_grad = True)
w2 = torch.randn(hiddenSize, outputSize, dtype = torch.float, requires_grad = True)
learningRate = 1e-6

model = torch.nn.Sequential(
        torch.nn.Linear(inputSize, hiddenSize),
        torch.nn.ReLU(),
        torch.nn.Linear(hiddenSize, outputSize))


loss_fn = torch.nn.MSELoss()
loss_values = []


for i in range(50):
    # Prolazak unaprijed (forward pass): izračunaj predikciju
    # Sada je potrebno samo ovo
    y_pred = model(x)
    
    
    # Nakon sto smo napravili jedan prolazak unaprijed idemo izracunati pogresku
    loss = loss_fn(y_pred,y )
    loss_values.append(loss.item())
    if i%10==0:
        print ('Pogreska u iteraciji {} iznosi {}'.format(i, loss.item()))
    
    # Sada ćemo napraviti backward propagaciju, izračunati gradijente
    # Prvo ćemo ih sve postaviti na 0
    model.zero_grad()
    # Poziv funkcije
    loss.backward()
    
    # Sada ćemo ažurirati težine w
    with torch.no_grad():
        # Proći ćemo kroz sve težine u našem modelu
        for w in model.parameters():
            w -= learningRate*w.grad
            
plt.plot(np.arange(0,50,1), loss_values)
plt.show()