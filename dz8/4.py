# -*- coding: utf-8 -*-
"""
Created on Thu Apr 30 14:12:23 2020

@author: David
"""
import numpy as np
import pandas as pd
import torch
import matplotlib.pyplot as plt

x = np.array([[3.3], [4.4], [5.5], [6.71], [6.93], [4.168], [9.779], [6.182], [7.59], [2.167], [7.042], [10.791], [5.313], [7.998], [3.1]], dtype=np.float32)
y = np.array([[1.7], [2.76], [2.09], [3.19], [2.094], [1.57], [3.366], [2.59], [2.82], [1.221], [2.821], [3.456], [1.65], [2.99], [1.3]], dtype=np.float32)
w1 = np.load('Podaci/w1.npy')
w2 = np.load('Podaci/w2.npy')

m=len(x)

def sigmoid(x):
    return 1.0/(1.0+np.exp(-x))

def forwardprop(x,y,w1,w2):
    a2=x.dot(w1)
    z2=sigmoid(a2)
    a3=z2.dot(w2)   
    return a3


def backwardprop(x,y,w1,w2):
    a3=forwardprop(x,y,w1,w2)
    d1 = a3-y
    alpha=0.001
    d2 = d1.dot(w2.T)*sigmoid(x)*(1-sigmoid(x))
    Dw2 = sigmoid(x).T.dot(d1)
    Dw1 = x.T.dot(d2)
    return w1 - (alpha)*Dw1, w2 - (alpha)*Dw2





for i in range(500):
    w1,w2=backwardprop(x,y,w1,w2)
a3=forwardprop(x, y, w1, w2)
