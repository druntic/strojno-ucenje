# -*- coding: utf-8 -*-
"""
Created on Fri Jun 19 17:22:48 2020

@author: David
"""

import numpy as np
import matplotlib.pyplot as plt
import sklearn
from sklearn.datasets import load_digits, load_iris
from sklearn.preprocessing import StandardScaler, scale
from sklearn.decomposition import PCA
#a
X = np.load('Podaci/Podaci-1.npy')
print(X)
plt.scatter(X[:,0],X[:,1])
u, s, vh = np.linalg.svd(X)
C = vh[0:1,:]
D = vh[0:1,:].T
X2=D.dot(X.dot(D).T).T
plt.scatter(X2[:,0],X2[:,1])
plt.show()
#print(X2)
#print(vh)
#print(vh[0:1,:])
print("pogreska: ",np.sum(np.linalg.norm(X-X2))*(np.linalg.norm(X-X2)))

#b
X = np.load('Podaci/Podaci-2.npy')
plt.scatter(X[:,0],X[:,1])
m=X.shape[0]
S = m*(X-X.mean(axis=0)).T.dot(X-X.mean(axis=0))
eigvalues,eigvectors = np.linalg.eig(S)
eigvectors=eigvectors.T
print(eigvalues[0])
print(eigvectors[0])

origin = [0,0]
plt.figure(2)
plt.quiver(*origin, *eigvectors[0],scale=5)
plt.show()