# -*- coding: utf-8 -*-
"""
Created on Sat Jun 20 00:28:43 2020

@author: David
"""

import numpy as np
import matplotlib.pyplot as plt
import sklearn
from sklearn.datasets import load_digits, load_iris
from sklearn.preprocessing import StandardScaler, scale
from sklearn.decomposition import PCA

iris = load_iris()['data']
pca = PCA(n_components=2)
iris2= pca.fit_transform(iris)


print("maksimalna varijanca: ", pca.explained_variance_)
print("smjerovi glavnih komponenti: ", pca.components_)
#plt.scatter(iris2[:,0],iris2[:,1])
tar=load_iris()['target']

cl=[0]
counter=1
for i in range(len(tar)-1):
    if(tar[i]!=tar[i+1]):
        counter+=1
        print(i)
        cl.append(i)
#print(iris2[1:len(iris2)])
#print(cl)
plt.scatter(iris2[0:49][:,0],iris2[0:49][:,1])
plt.scatter(iris2[50:99][:,0],iris2[50:99][:,1])
plt.scatter(iris2[99:len(iris2)][:,0],iris2[99:len(iris2)][:,1])
plt.show()
#nakon redukcije podaci su i dalje separirani