# -*- coding: utf-8 -*-
"""
Created on Fri Jun 19 23:02:23 2020

@author: David
"""
#source code: https://jakevdp.github.io/PythonDataScienceHandbook/05.09-principal-component-analysis.html
import numpy as np
import matplotlib.pyplot as plt
import sklearn
from sklearn.datasets import load_digits, load_iris
from sklearn.preprocessing import StandardScaler, scale
from sklearn.decomposition import PCA
digits = load_digits()
print(digits['data'].shape)

def plot_digits(data):
    fig, axes = plt.subplots(4, 10, figsize=(10, 4),
                             subplot_kw={'xticks':[], 'yticks':[]},
                             gridspec_kw=dict(hspace=0.1, wspace=0.1))
    for i, ax in enumerate(axes.flat):
        ax.imshow(data[i].reshape(8, 8),
                  cmap='binary', interpolation='nearest',
                  clim=(0, 16))
plot_digits(digits.data)
plt.figure(2)
pca = PCA().fit(digits.data)
plt.plot(np.cumsum(pca.explained_variance_ratio_))
plt.xlabel('number of components')
plt.ylabel('cumulative explained variance');

#za k=40 imat cemo i dalje jako velikih % varijance
k=40
u, s, vh = np.linalg.svd(digits['data'])
C = vh[0:k,:]
D = vh[0:k,:].T
digits2=D.dot(digits['data'].dot(D).T).T
plot_digits(digits2)






