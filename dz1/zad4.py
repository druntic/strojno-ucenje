# -*- coding: utf-8 -*-
"""
Created on Mon Mar  2 23:27:53 2020

@author: David
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import scale
from sklearn import linear_model


files = pd.read_csv (r'Podaci\house_train.csv')
files=files.dropna()

y=np.array(files['price'])
sqft=np.array(files['sqft_living'])
grade=np.array(files['grade'])
bedrooms=np.array(files['bedrooms'])


x=np.column_stack((sqft,grade,bedrooms))
print(x)
reg=linear_model.LinearRegression()
reg.fit(x,y)
print(reg.coef_)