# -*- coding: utf-8 -*-
"""
Created on Sat Feb 29 21:15:55 2020

@author: David
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import scale

fig = plt.figure()
ax = plt.axes()
x = np.linspace(0, 10, 1000)
plt.plot(x, 2*x + 1)