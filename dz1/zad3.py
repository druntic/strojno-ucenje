# -*- coding: utf-8 -*-
"""
Created on Sat Feb 29 12:44:21 2020

@author: David
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import scale
from sklearn import linear_model

fig = plt.figure()
ax = plt.axes() 


files = pd.read_csv (r'Podaci\house_train.csv')
files=files.dropna()
#a
sqft=scale(files['sqft_living'])
price=scale(files['price'])

y=files['price']
#plt.xlabel("sqrt")
#plt.ylabel("price")


m=len(files)#broj podataka

#b
x=[]
y=files['price']

for i in sqft:
    x.append([1,i])
x=np.matrix(x)
print(x)

#c

plt.scatter(sqft,price)
plt.show()


#d
theta1=1
theta2=1
alpha=0.01
iterations=100
m=len(files)#broj podataka
y=np.array(scale(files['price']))
x1=np.array(scale(files['sqft_living']))
for i in range (iterations):
    h=theta1*x1+theta2
    newtheta1=-(1/m)*sum(x1*(y-h))
    newtheta2=-(1/m)*sum((y-h))
    theta1=theta1-alpha*newtheta1
    theta2=theta2-alpha*newtheta2
    cost = (1/m) * sum([val**2 for val in (y-h)])#treba se smanjivati
    print(cost)
#g (stavio sam ga ovdje jer sam zelio odmah usporediti moje vrijednosti s vrijednostima implementirane funkcije)
reg=linear_model.LinearRegression()
reg.fit(x,y)
coef=reg.coef_[1]
intercept=reg.intercept_
print(coef,intercept)

#e
print(theta1,theta2)
plt.scatter(sqft,price)
x = np.linspace(0, 10, 1000)
plt.plot(x, x*theta1+theta2, color="red" )
plt.scatter(sqft,price)
plt.show()
#f
files_test = pd.read_csv (r'Podaci\house_test.csv')
files_test=files_test.dropna()
sqft_test=scale(files['sqft_living'])
price_test=scale(files['price'])
sqft_test=np.array(sqft_test)
price_test=np.array(price_test)
plt.scatter(sqft_test,price_test)
x = np.linspace(0, 10, 1000)
plt.plot(x, x*theta1+theta2, color="blue" )
plt.show()

#h
x_test=[]
for i in sqft_test:
    x_test.append([1,i])
x_test=np.matrix(x_test)
print(x_test)
print("Predictions:  ", reg.predict(x_test))#y predicted
plt.scatter(sqft_test,price_test, color="blue")
plt.scatter(sqft_test,reg.predict(x_test), color="red")
plt.plot(x, x*reg.coef_[1]+reg.coef_[0], color="purple" )
plt.plot(x, x*theta1+theta2, color="green" )
plt.show()