# -*- coding: utf-8 -*-
"""
Created on Fri May 29 15:08:31 2020

@author: David
"""
import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
from scipy.special import binom
import pandas as pd
from numpy import cov
import scipy.linalg as la
X = np.array([[8.7,-0.8,-3.2,0.6],[-5.5,0.3,-0.8,0.1],[9.5,-0.7,-2.8,-0.1],[0.3,2.4,1.2,0.4],[1.0,2.1,0.001,0],[3.6,0.3,1.2,-1.2]])

mi = np.mean(X,axis=0)
print(mi)

sig = (X-mi).T.dot((X-mi))/len(X)
print(sig)
#matrica sig mora biti simetrica i sve svojstvene vrijednosti moraju biti vece od 0
evals, evecs = la.eig(sig)
print(evals.real)
#matrica odgovara tim svojstvima

