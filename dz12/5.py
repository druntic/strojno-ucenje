# -*- coding: utf-8 -*-
"""
Created on Fri May 29 18:26:50 2020

@author: David
"""
import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
from scipy.special import binom
import pandas as pd
from numpy import cov
import scipy.linalg as la

X = pd.read_csv("Podaci/djelatnici.csv",';',decimal = ',').to_numpy()
#print(X[:,4])
#print(X[:,6])
xheight=np.array(X[:,4])
xsalary=np.array(X[:,6])
miheight=np.mean(X[:,4])
misalary=np.mean(X[:,6])
sig2height=1/len(X[:,4])*np.sum((X[:,4]-miheight)*(X[:,4]-miheight))
sig2salary=1/len(X[:,6])*np.sum((X[:,6]-miheight)*(X[:,6]-miheight))
print(sig2height)
#plt.hist([X[:,4], bins="auto")



plt.hist(xheight, bins = "auto") 
plt.title("visina") 
plt.show()


plt.hist(xsalary, bins = "auto") 
plt.title("placa") 
plt.show()

#visina ima normalnu distribuciju dok placa nema